﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class 学生信息查询
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.TableLayoutPanel1 = New System.Windows.Forms.TableLayoutPanel()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.人数label = New System.Windows.Forms.Label()
        Me.TextBox4 = New System.Windows.Forms.TextBox()
        Me.姓名label = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.学号label = New System.Windows.Forms.Label()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.JXGL2320601543DataSet = New WindowsApplication1.JXGL2320601543DataSet()
        Me.SBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.STableAdapter = New WindowsApplication1.JXGL2320601543DataSetTableAdapters.STableAdapter()
        Me.SDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SNAMEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SSEXDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SBIRTHINDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.PLACEOFBDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.SCODEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CLASSDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.TableLayoutPanel2 = New System.Windows.Forms.TableLayoutPanel()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.TableLayoutPanel1.SuspendLayout()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JXGL2320601543DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.TableLayoutPanel2.SuspendLayout()
        Me.SuspendLayout()
        '
        'TableLayoutPanel1
        '
        Me.TableLayoutPanel1.ColumnCount = 1
        Me.TableLayoutPanel1.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle())
        Me.TableLayoutPanel1.Controls.Add(Me.GroupBox1, 0, 0)
        Me.TableLayoutPanel1.Controls.Add(Me.DataGridView1, 0, 1)
        Me.TableLayoutPanel1.Controls.Add(Me.TableLayoutPanel2, 0, 2)
        Me.TableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.TableLayoutPanel1.Name = "TableLayoutPanel1"
        Me.TableLayoutPanel1.RowCount = 3
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel1.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 100.0!))
        Me.TableLayoutPanel1.Size = New System.Drawing.Size(845, 511)
        Me.TableLayoutPanel1.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.人数label)
        Me.GroupBox1.Controls.Add(Me.TextBox4)
        Me.GroupBox1.Controls.Add(Me.姓名label)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.学号label)
        Me.GroupBox1.Location = New System.Drawing.Point(3, 3)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(839, 94)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = False
        '
        'TextBox3
        '
        Me.TextBox3.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox3.Location = New System.Drawing.Point(556, 40)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.ReadOnly = True
        Me.TextBox3.Size = New System.Drawing.Size(100, 25)
        Me.TextBox3.TabIndex = 1
        '
        '人数label
        '
        Me.人数label.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.人数label.AutoSize = True
        Me.人数label.Location = New System.Drawing.Point(479, 46)
        Me.人数label.Name = "人数label"
        Me.人数label.Size = New System.Drawing.Size(37, 15)
        Me.人数label.TabIndex = 0
        Me.人数label.Text = "人数"
        '
        'TextBox4
        '
        Me.TextBox4.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox4.Location = New System.Drawing.Point(327, 43)
        Me.TextBox4.Name = "TextBox4"
        Me.TextBox4.Size = New System.Drawing.Size(100, 25)
        Me.TextBox4.TabIndex = 1
        '
        '姓名label
        '
        Me.姓名label.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.姓名label.AutoSize = True
        Me.姓名label.Location = New System.Drawing.Point(265, 43)
        Me.姓名label.Name = "姓名label"
        Me.姓名label.Size = New System.Drawing.Size(37, 15)
        Me.姓名label.TabIndex = 0
        Me.姓名label.Text = "姓名"
        '
        'TextBox1
        '
        Me.TextBox1.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.TextBox1.Location = New System.Drawing.Point(129, 40)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.Size = New System.Drawing.Size(100, 25)
        Me.TextBox1.TabIndex = 1
        '
        '学号label
        '
        Me.学号label.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.学号label.AutoSize = True
        Me.学号label.Location = New System.Drawing.Point(73, 43)
        Me.学号label.Name = "学号label"
        Me.学号label.Size = New System.Drawing.Size(37, 15)
        Me.学号label.TabIndex = 0
        Me.学号label.Text = "学号"
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.SDataGridViewTextBoxColumn, Me.SNAMEDataGridViewTextBoxColumn, Me.SSEXDataGridViewTextBoxColumn, Me.SBIRTHINDataGridViewTextBoxColumn, Me.PLACEOFBDataGridViewTextBoxColumn, Me.SCODEDataGridViewTextBoxColumn, Me.CLASSDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.SBindingSource
        Me.DataGridView1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.DataGridView1.Location = New System.Drawing.Point(3, 103)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersWidth = 51
        Me.DataGridView1.RowTemplate.Height = 27
        Me.DataGridView1.Size = New System.Drawing.Size(839, 199)
        Me.DataGridView1.TabIndex = 1
        '
        'JXGL2320601543DataSet
        '
        Me.JXGL2320601543DataSet.DataSetName = "JXGL2320601543DataSet"
        Me.JXGL2320601543DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SBindingSource
        '
        Me.SBindingSource.DataMember = "S"
        Me.SBindingSource.DataSource = Me.JXGL2320601543DataSet
        '
        'STableAdapter
        '
        Me.STableAdapter.ClearBeforeFill = True
        '
        'SDataGridViewTextBoxColumn
        '
        Me.SDataGridViewTextBoxColumn.DataPropertyName = "S#"
        Me.SDataGridViewTextBoxColumn.HeaderText = "S#"
        Me.SDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.SDataGridViewTextBoxColumn.Name = "SDataGridViewTextBoxColumn"
        Me.SDataGridViewTextBoxColumn.Width = 125
        '
        'SNAMEDataGridViewTextBoxColumn
        '
        Me.SNAMEDataGridViewTextBoxColumn.DataPropertyName = "SNAME"
        Me.SNAMEDataGridViewTextBoxColumn.HeaderText = "SNAME"
        Me.SNAMEDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.SNAMEDataGridViewTextBoxColumn.Name = "SNAMEDataGridViewTextBoxColumn"
        Me.SNAMEDataGridViewTextBoxColumn.Width = 125
        '
        'SSEXDataGridViewTextBoxColumn
        '
        Me.SSEXDataGridViewTextBoxColumn.DataPropertyName = "SSEX"
        Me.SSEXDataGridViewTextBoxColumn.HeaderText = "SSEX"
        Me.SSEXDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.SSEXDataGridViewTextBoxColumn.Name = "SSEXDataGridViewTextBoxColumn"
        Me.SSEXDataGridViewTextBoxColumn.Width = 125
        '
        'SBIRTHINDataGridViewTextBoxColumn
        '
        Me.SBIRTHINDataGridViewTextBoxColumn.DataPropertyName = "SBIRTHIN"
        Me.SBIRTHINDataGridViewTextBoxColumn.HeaderText = "SBIRTHIN"
        Me.SBIRTHINDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.SBIRTHINDataGridViewTextBoxColumn.Name = "SBIRTHINDataGridViewTextBoxColumn"
        Me.SBIRTHINDataGridViewTextBoxColumn.Width = 125
        '
        'PLACEOFBDataGridViewTextBoxColumn
        '
        Me.PLACEOFBDataGridViewTextBoxColumn.DataPropertyName = "PLACEOFB"
        Me.PLACEOFBDataGridViewTextBoxColumn.HeaderText = "PLACEOFB"
        Me.PLACEOFBDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.PLACEOFBDataGridViewTextBoxColumn.Name = "PLACEOFBDataGridViewTextBoxColumn"
        Me.PLACEOFBDataGridViewTextBoxColumn.Width = 125
        '
        'SCODEDataGridViewTextBoxColumn
        '
        Me.SCODEDataGridViewTextBoxColumn.DataPropertyName = "SCODE#"
        Me.SCODEDataGridViewTextBoxColumn.HeaderText = "SCODE#"
        Me.SCODEDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.SCODEDataGridViewTextBoxColumn.Name = "SCODEDataGridViewTextBoxColumn"
        Me.SCODEDataGridViewTextBoxColumn.Width = 125
        '
        'CLASSDataGridViewTextBoxColumn
        '
        Me.CLASSDataGridViewTextBoxColumn.DataPropertyName = "CLASS"
        Me.CLASSDataGridViewTextBoxColumn.HeaderText = "CLASS"
        Me.CLASSDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.CLASSDataGridViewTextBoxColumn.Name = "CLASSDataGridViewTextBoxColumn"
        Me.CLASSDataGridViewTextBoxColumn.Width = 125
        '
        'TableLayoutPanel2
        '
        Me.TableLayoutPanel2.ColumnCount = 5
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.98951!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.02099!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120.0!))
        Me.TableLayoutPanel2.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 34.98951!))
        Me.TableLayoutPanel2.Controls.Add(Me.Button1, 1, 0)
        Me.TableLayoutPanel2.Controls.Add(Me.Button2, 3, 0)
        Me.TableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill
        Me.TableLayoutPanel2.Location = New System.Drawing.Point(13, 323)
        Me.TableLayoutPanel2.Name = "TableLayoutPanel2"
        Me.TableLayoutPanel2.RowCount = 1
        Me.TableLayoutPanel2.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me.TableLayoutPanel2.Size = New System.Drawing.Size(811, 176)
        Me.TableLayoutPanel2.TabIndex = 2
        '
        'Button1
        '
        Me.Button1.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button1.AutoEllipsis = True
        Me.Button1.AutoSize = True
        Me.Button1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.Button1.Location = New System.Drawing.Point(288, 3)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(114, 170)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "查询"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Anchor = CType((((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Bottom) _
            Or System.Windows.Forms.AnchorStyles.Left) _
            Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.Button2.Location = New System.Drawing.Point(408, 3)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(114, 170)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "取消"
        Me.Button2.UseVisualStyleBackColor = True
        '
        '学生信息查询
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(845, 511)
        Me.Controls.Add(Me.TableLayoutPanel1)
        Me.Name = "学生信息查询"
        Me.Text = "学生信息查询"
        Me.TableLayoutPanel1.ResumeLayout(False)
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JXGL2320601543DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.TableLayoutPanel2.ResumeLayout(False)
        Me.TableLayoutPanel2.PerformLayout()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents TableLayoutPanel1 As TableLayoutPanel
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents 学号label As Label
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents 姓名label As Label
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents 人数label As Label
    Friend WithEvents TextBox4 As TextBox
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents JXGL2320601543DataSet As JXGL2320601543DataSet
    Friend WithEvents SBindingSource As BindingSource
    Friend WithEvents STableAdapter As JXGL2320601543DataSetTableAdapters.STableAdapter
    Friend WithEvents SDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SNAMEDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SSEXDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SBIRTHINDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents PLACEOFBDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents SCODEDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CLASSDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents TableLayoutPanel2 As TableLayoutPanel
    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
End Class

﻿Imports System.Data.SqlClient
Public Class 成绩信息维护窗体

    Private str As String = "Data Source=HHHHH;Initial Catalog=JXGL2320601543;Integrated Security=True"
    Private conn As New SqlConnection(str)
    Private num As Integer = 0


    Private Sub Form2_Load(sender As Object, e As EventArgs) Handles MyBase.Load
        'TODO: 这行代码将数据加载到表“JXGL2320601543DataSet1.SC”中。您可以根据需要移动或删除它。
        Me.SCTableAdapter.Fill(Me.JXGL2320601543DataSet1.SC)
        'TODO: 这行代码将数据加载到表“JXGL2320601543DataSet.S”中。您可以根据需要移动或删除它。
        'Me.STableAdapter.Fill(Me.JXGL2320601543DataSet.S)

    End Sub

    Private Sub DataGridView1_CellDoubleClick(sender As System.Object, e As System.Windows.Forms.DataGridViewCellEventArgs) Handles DataGridView1.CellDoubleClick
        '双击DataGridView选中需要进行更新或删除的数据
        TextBox1.Text = DataGridView1.Rows(e.RowIndex).Cells(1).Value.ToString
        TextBox2.Text = DataGridView1.Rows(e.RowIndex).Cells(2).Value.ToString
        TextBox3.Text = DataGridView1.Rows(e.RowIndex).Cells(3).Value.ToString
        DataGridView1.Rows(e.RowIndex).Cells(0).Value = True
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        '更新双击DataGridView中选中的数据
        If TextBox1.Text <> "" And TextBox2.Text <> "" Then
            Dim sqlstring As String
            sqlstring = "update sc set grade='" & TextBox3.Text & " ' where s#='" & TextBox1.Text & "' and c#='" & TextBox2.Text & "'"
            Dim cmd As New SqlCommand(sqlstring, conn)
            conn.Open()
            cmd.ExecuteNonQuery()
            MsgBox("数据更新成功！")
            conn.Close()

            Call Form2_Load(Nothing, Nothing)
            TextBox1.Text = ""
            TextBox2.Text = ""
            TextBox3.Text = ""
        Else
            MsgBox("请双击数据表中要更新的行！")
        End If
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        '删除选中的数据行
        Dim cellvalue As Object = False       '定义checkbox选中事项，并初始化
        Dim i, j As Integer
        Dim num As Integer               '定义保存SC表中总行数的变量
        num = Me.DataGridView1.RowCount     '从DataGridView中计算数据总行数，并赋给num
        Dim a(num) As Boolean               '定义数组a(num)保存“标记”列选中行的位置数
        For i = 0 To num - 1                '初始化数组a(num)
            a(i) = False
        Next
        For j = 0 To num - 2          'DataGridView最后一行为空，从0开始循环，至num-2结束
            cellvalue = Me.DataGridView1.Rows(j).Cells(0).Value
            If cellvalue = True Then           '如果“标记”中当前行被选中，则将其记入数据a(j)中
                a(j) = True
                cellvalue = False
            End If
        Next
        Dim str1 As String            '定义游标cc1，用于删除选中“标记”行的元组
        str1 = ""
        str1 = str1 & "declare cc1 cursor" & " "
        str1 = str1 & "for select * from sc" & " "
        str1 = str1 & "open cc1" & " "
        Dim x As Integer = 0
        While x < num - 1
            str1 = str1 & "fetch next from cc1 "
            If a(x) = True Then       '当数组a(x)中的值为true时，即“标记”被选中时，则删除该行
                str1 = str1 & "delete from sc" & " "
                str1 = str1 & "where current of cc1" & " "
            End If
            x = x + 1
        End While
        Select Case MsgBox("您确定要删除标记的数据吗？", MsgBoxStyle.OkCancel)
            Case MsgBoxResult.Ok
                Dim sqlcmd As New SqlCommand(str1, conn)
                conn.Open()
                sqlcmd.ExecuteNonQuery()            '执行删除
                conn.Close()
                Call Form2_Load(Nothing, Nothing)   '调用窗体加载事件，即刷新数据
                TextBox1.Text = ""
                TextBox2.Text = ""
                TextBox3.Text = ""
        End Select
    End Sub

End Class
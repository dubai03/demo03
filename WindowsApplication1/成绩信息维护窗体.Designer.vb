﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class 成绩信息维护窗体
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.TextBox1 = New System.Windows.Forms.TextBox()
        Me.TextBox2 = New System.Windows.Forms.TextBox()
        Me.TextBox3 = New System.Windows.Forms.TextBox()
        Me.DataGridView1 = New System.Windows.Forms.DataGridView()
        Me.JXGL2320601543DataSet = New WindowsApplication1.JXGL2320601543DataSet()
        Me.JXGL2320601543DataSetBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.STableAdapter = New WindowsApplication1.JXGL2320601543DataSetTableAdapters.STableAdapter()
        Me.JXGL2320601543DataSet1 = New WindowsApplication1.JXGL2320601543DataSet1()
        Me.SCBindingSource = New System.Windows.Forms.BindingSource(Me.components)
        Me.SCTableAdapter = New WindowsApplication1.JXGL2320601543DataSet1TableAdapters.SCTableAdapter()
        Me.标记 = New System.Windows.Forms.DataGridViewCheckBoxColumn()
        Me.SDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.CDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GRADEDataGridViewTextBoxColumn = New System.Windows.Forms.DataGridViewTextBoxColumn()
        Me.GroupBox1.SuspendLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JXGL2320601543DataSet, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JXGL2320601543DataSetBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.JXGL2320601543DataSet1, System.ComponentModel.ISupportInitialize).BeginInit()
        CType(Me.SCBindingSource, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(101, 404)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(160, 45)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "更新"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(334, 404)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(153, 45)
        Me.Button2.TabIndex = 1
        Me.Button2.Text = "删除"
        Me.Button2.UseVisualStyleBackColor = True
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.TextBox3)
        Me.GroupBox1.Controls.Add(Me.TextBox2)
        Me.GroupBox1.Controls.Add(Me.TextBox1)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Location = New System.Drawing.Point(43, 270)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Size = New System.Drawing.Size(630, 100)
        Me.GroupBox1.TabIndex = 2
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "当前记录"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(240, 41)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(52, 15)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "课程号"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(32, 41)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(37, 15)
        Me.Label2.TabIndex = 0
        Me.Label2.Text = "学号"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(407, 44)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(37, 15)
        Me.Label3.TabIndex = 0
        Me.Label3.Text = "成绩"
        '
        'TextBox1
        '
        Me.TextBox1.Location = New System.Drawing.Point(94, 41)
        Me.TextBox1.Name = "TextBox1"
        Me.TextBox1.ReadOnly = True
        Me.TextBox1.Size = New System.Drawing.Size(100, 25)
        Me.TextBox1.TabIndex = 1
        '
        'TextBox2
        '
        Me.TextBox2.Location = New System.Drawing.Point(291, 41)
        Me.TextBox2.Name = "TextBox2"
        Me.TextBox2.ReadOnly = True
        Me.TextBox2.Size = New System.Drawing.Size(100, 25)
        Me.TextBox2.TabIndex = 1
        '
        'TextBox3
        '
        Me.TextBox3.Location = New System.Drawing.Point(468, 38)
        Me.TextBox3.Name = "TextBox3"
        Me.TextBox3.Size = New System.Drawing.Size(100, 25)
        Me.TextBox3.TabIndex = 1
        '
        'DataGridView1
        '
        Me.DataGridView1.AutoGenerateColumns = False
        Me.DataGridView1.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize
        Me.DataGridView1.Columns.AddRange(New System.Windows.Forms.DataGridViewColumn() {Me.标记, Me.SDataGridViewTextBoxColumn, Me.CDataGridViewTextBoxColumn, Me.GRADEDataGridViewTextBoxColumn})
        Me.DataGridView1.DataSource = Me.SCBindingSource
        Me.DataGridView1.Location = New System.Drawing.Point(43, 49)
        Me.DataGridView1.Name = "DataGridView1"
        Me.DataGridView1.RowHeadersWidth = 51
        Me.DataGridView1.RowTemplate.Height = 27
        Me.DataGridView1.Size = New System.Drawing.Size(595, 198)
        Me.DataGridView1.TabIndex = 3
        '
        'JXGL2320601543DataSet
        '
        Me.JXGL2320601543DataSet.DataSetName = "JXGL2320601543DataSet"
        Me.JXGL2320601543DataSet.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'JXGL2320601543DataSetBindingSource
        '
        Me.JXGL2320601543DataSetBindingSource.DataSource = Me.JXGL2320601543DataSet
        Me.JXGL2320601543DataSetBindingSource.Position = 0
        '
        'SBindingSource
        '
        Me.SBindingSource.DataMember = "S"
        Me.SBindingSource.DataSource = Me.JXGL2320601543DataSetBindingSource
        '
        'STableAdapter
        '
        Me.STableAdapter.ClearBeforeFill = True
        '
        'JXGL2320601543DataSet1
        '
        Me.JXGL2320601543DataSet1.DataSetName = "JXGL2320601543DataSet1"
        Me.JXGL2320601543DataSet1.SchemaSerializationMode = System.Data.SchemaSerializationMode.IncludeSchema
        '
        'SCBindingSource
        '
        Me.SCBindingSource.DataMember = "SC"
        Me.SCBindingSource.DataSource = Me.JXGL2320601543DataSet1
        '
        'SCTableAdapter
        '
        Me.SCTableAdapter.ClearBeforeFill = True
        '
        '标记
        '
        Me.标记.HeaderText = "标记"
        Me.标记.MinimumWidth = 6
        Me.标记.Name = "标记"
        Me.标记.Width = 125
        '
        'SDataGridViewTextBoxColumn
        '
        Me.SDataGridViewTextBoxColumn.DataPropertyName = "S#"
        Me.SDataGridViewTextBoxColumn.HeaderText = "学号"
        Me.SDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.SDataGridViewTextBoxColumn.Name = "SDataGridViewTextBoxColumn"
        Me.SDataGridViewTextBoxColumn.Width = 125
        '
        'CDataGridViewTextBoxColumn
        '
        Me.CDataGridViewTextBoxColumn.DataPropertyName = "C#"
        Me.CDataGridViewTextBoxColumn.HeaderText = "课程号"
        Me.CDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.CDataGridViewTextBoxColumn.Name = "CDataGridViewTextBoxColumn"
        Me.CDataGridViewTextBoxColumn.Width = 125
        '
        'GRADEDataGridViewTextBoxColumn
        '
        Me.GRADEDataGridViewTextBoxColumn.DataPropertyName = "GRADE"
        Me.GRADEDataGridViewTextBoxColumn.HeaderText = "成绩"
        Me.GRADEDataGridViewTextBoxColumn.MinimumWidth = 6
        Me.GRADEDataGridViewTextBoxColumn.Name = "GRADEDataGridViewTextBoxColumn"
        Me.GRADEDataGridViewTextBoxColumn.Width = 125
        '
        '成绩信息维护窗体
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(685, 461)
        Me.Controls.Add(Me.DataGridView1)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.Button1)
        Me.Name = "成绩信息维护窗体"
        Me.Text = "Form2"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        CType(Me.DataGridView1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JXGL2320601543DataSet, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JXGL2320601543DataSetBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.JXGL2320601543DataSet1, System.ComponentModel.ISupportInitialize).EndInit()
        CType(Me.SCBindingSource, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)

    End Sub

    Friend WithEvents Button1 As Button
    Friend WithEvents Button2 As Button
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents TextBox3 As TextBox
    Friend WithEvents TextBox2 As TextBox
    Friend WithEvents TextBox1 As TextBox
    Friend WithEvents Label2 As Label
    Friend WithEvents Label3 As Label
    Friend WithEvents Label1 As Label
    Friend WithEvents DataGridView1 As DataGridView
    Friend WithEvents JXGL2320601543DataSetBindingSource As BindingSource
    Friend WithEvents JXGL2320601543DataSet As JXGL2320601543DataSet
    Friend WithEvents SBindingSource As BindingSource
    Friend WithEvents STableAdapter As JXGL2320601543DataSetTableAdapters.STableAdapter
    Friend WithEvents JXGL2320601543DataSet1 As JXGL2320601543DataSet1
    Friend WithEvents SCBindingSource As BindingSource
    Friend WithEvents SCTableAdapter As JXGL2320601543DataSet1TableAdapters.SCTableAdapter
    Friend WithEvents 标记 As DataGridViewCheckBoxColumn
    Friend WithEvents SDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents CDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
    Friend WithEvents GRADEDataGridViewTextBoxColumn As DataGridViewTextBoxColumn
End Class

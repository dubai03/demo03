﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class 主窗口
    Inherits System.Windows.Forms.Form

    'Form 重写 Dispose，以清理组件列表。
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Windows 窗体设计器所必需的
    Private components As System.ComponentModel.IContainer

    '注意: 以下过程是 Windows 窗体设计器所必需的
    '可以使用 Windows 窗体设计器修改它。  
    '不要使用代码编辑器修改它。
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.MenuStrip1 = New System.Windows.Forms.MenuStrip()
        Me.学生信息管理ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.学生信息添加ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.学生信息查询ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.课程信息管理ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.成绩信息管理ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.系统管理ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.关于ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.退出ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.成绩信息查询ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.成绩信息维护ToolStripMenuItem = New System.Windows.Forms.ToolStripMenuItem()
        Me.PictureBox1 = New System.Windows.Forms.PictureBox()
        Me.MenuStrip1.SuspendLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        'MenuStrip1
        '
        Me.MenuStrip1.ImageScalingSize = New System.Drawing.Size(20, 20)
        Me.MenuStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.学生信息管理ToolStripMenuItem, Me.课程信息管理ToolStripMenuItem, Me.成绩信息管理ToolStripMenuItem, Me.系统管理ToolStripMenuItem, Me.关于ToolStripMenuItem, Me.退出ToolStripMenuItem})
        Me.MenuStrip1.Location = New System.Drawing.Point(0, 0)
        Me.MenuStrip1.Name = "MenuStrip1"
        Me.MenuStrip1.Size = New System.Drawing.Size(800, 30)
        Me.MenuStrip1.TabIndex = 0
        Me.MenuStrip1.Text = "MenuStrip1"
        '
        '学生信息管理ToolStripMenuItem
        '
        Me.学生信息管理ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.学生信息添加ToolStripMenuItem, Me.学生信息查询ToolStripMenuItem})
        Me.学生信息管理ToolStripMenuItem.Name = "学生信息管理ToolStripMenuItem"
        Me.学生信息管理ToolStripMenuItem.Size = New System.Drawing.Size(113, 24)
        Me.学生信息管理ToolStripMenuItem.Text = "学生信息管理"
        '
        '学生信息添加ToolStripMenuItem
        '
        Me.学生信息添加ToolStripMenuItem.Name = "学生信息添加ToolStripMenuItem"
        Me.学生信息添加ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.学生信息添加ToolStripMenuItem.Text = "学生信息添加"
        '
        '学生信息查询ToolStripMenuItem
        '
        Me.学生信息查询ToolStripMenuItem.Name = "学生信息查询ToolStripMenuItem"
        Me.学生信息查询ToolStripMenuItem.Size = New System.Drawing.Size(182, 26)
        Me.学生信息查询ToolStripMenuItem.Text = "学生信息查询"
        '
        '课程信息管理ToolStripMenuItem
        '
        Me.课程信息管理ToolStripMenuItem.Name = "课程信息管理ToolStripMenuItem"
        Me.课程信息管理ToolStripMenuItem.Size = New System.Drawing.Size(113, 24)
        Me.课程信息管理ToolStripMenuItem.Text = "课程信息管理"
        '
        '成绩信息管理ToolStripMenuItem
        '
        Me.成绩信息管理ToolStripMenuItem.DropDownItems.AddRange(New System.Windows.Forms.ToolStripItem() {Me.成绩信息查询ToolStripMenuItem, Me.成绩信息维护ToolStripMenuItem})
        Me.成绩信息管理ToolStripMenuItem.Name = "成绩信息管理ToolStripMenuItem"
        Me.成绩信息管理ToolStripMenuItem.Size = New System.Drawing.Size(113, 24)
        Me.成绩信息管理ToolStripMenuItem.Text = "成绩信息管理"
        '
        '系统管理ToolStripMenuItem
        '
        Me.系统管理ToolStripMenuItem.Name = "系统管理ToolStripMenuItem"
        Me.系统管理ToolStripMenuItem.Size = New System.Drawing.Size(83, 24)
        Me.系统管理ToolStripMenuItem.Text = "系统管理"
        '
        '关于ToolStripMenuItem
        '
        Me.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem"
        Me.关于ToolStripMenuItem.Size = New System.Drawing.Size(53, 24)
        Me.关于ToolStripMenuItem.Text = "关于"
        '
        '退出ToolStripMenuItem
        '
        Me.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem"
        Me.退出ToolStripMenuItem.Size = New System.Drawing.Size(53, 24)
        Me.退出ToolStripMenuItem.Text = "退出"
        '
        '成绩信息查询ToolStripMenuItem
        '
        Me.成绩信息查询ToolStripMenuItem.Name = "成绩信息查询ToolStripMenuItem"
        Me.成绩信息查询ToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.成绩信息查询ToolStripMenuItem.Text = "成绩信息查询"
        '
        '成绩信息维护ToolStripMenuItem
        '
        Me.成绩信息维护ToolStripMenuItem.Name = "成绩信息维护ToolStripMenuItem"
        Me.成绩信息维护ToolStripMenuItem.Size = New System.Drawing.Size(224, 26)
        Me.成绩信息维护ToolStripMenuItem.Text = "成绩信息维护"
        '
        'PictureBox1
        '
        Me.PictureBox1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.PictureBox1.Image = Global.WindowsApplication1.My.Resources.Resources.LOGIN
        Me.PictureBox1.Location = New System.Drawing.Point(0, 30)
        Me.PictureBox1.Name = "PictureBox1"
        Me.PictureBox1.Size = New System.Drawing.Size(800, 420)
        Me.PictureBox1.TabIndex = 1
        Me.PictureBox1.TabStop = False
        '
        '主窗口
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(8.0!, 15.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(800, 450)
        Me.Controls.Add(Me.PictureBox1)
        Me.Controls.Add(Me.MenuStrip1)
        Me.MainMenuStrip = Me.MenuStrip1
        Me.Name = "主窗口"
        Me.Text = "某大学信息管理系统"
        Me.MenuStrip1.ResumeLayout(False)
        Me.MenuStrip1.PerformLayout()
        CType(Me.PictureBox1, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub

    Friend WithEvents MenuStrip1 As MenuStrip
    Friend WithEvents 学生信息管理ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 学生信息添加ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 学生信息查询ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 课程信息管理ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 成绩信息管理ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 系统管理ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 关于ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 退出ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 成绩信息查询ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents 成绩信息维护ToolStripMenuItem As ToolStripMenuItem
    Friend WithEvents PictureBox1 As PictureBox
End Class

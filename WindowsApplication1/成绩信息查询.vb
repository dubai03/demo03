﻿Imports System.Data.SqlClient
Public Class 成绩信息查询
    Dim connstr As String = "Data Source =HHHHH;Initial Catalog = JXGL2320601543;Integrated Security = True"
    Dim conn As New SqlClient.SqlConnection(connstr)

    Private Sub 成绩信息查询_Load(sender As System.Object, e As System.EventArgs) Handles MyBase.Load
        '窗体初始化时，为组合框中放入数据
        '为学生查询组合框添加项
        Dim sel As String = "select * from S"
        Dim com As New SqlCommand(sel, conn)
        conn.Open()
        Dim sreader As SqlDataReader = com.ExecuteReader
        Do While sreader.Read
            ComboBox1.Items.Add(sreader.GetString(0) & "  " & sreader.GetString(1))
        Loop
        conn.Close()

        '为课程查询组合框添加项
        Dim sel1 As String = "select * from c"
        Dim com1 As New SqlCommand(sel1, conn)
        conn.Open()
        Dim sreader1 As SqlDataReader = com1.ExecuteReader
        Do While sreader1.Read
            ComboBox2.Items.Add(sreader1.GetString(0) & "  " & sreader1.GetString(1))
        Loop
        conn.Close()
    End Sub

    Private Sub Button1_Click(sender As System.Object, e As System.EventArgs) Handles Button1.Click
        '执行查询命令
        Dim snum As String = Strings.Left(ComboBox1.Text, 9)     '学号为9位，因此查询时取组合框前9位
        Dim cnum As String = Strings.Left(ComboBox2.Text, 7)    '课程号为7位，因此查询时取组合框前7位
        Dim sqlstring As String = ""
        ListView1.Items.Clear()
        If snum <> "" Then
            If cnum <> "" Then
                sqlstring = "select s.s#,s.sname,c.c#,c.cname,sc.grade from S,SC,C where sc.s#='" & snum & "' and sc.c#='" & cnum & "' and S.s#=SC.s# and SC.c#=C.c#"
            Else
                sqlstring = "select s.s#,s.sname,c.c#,c.cname,sc.grade from S,SC,C where sc.s#='" & snum & "' and S.s#=SC.s# and SC.c#=C.c#"
            End If
        Else
            If cnum <> "" Then
                sqlstring = "select s.s#,s.sname,c.c#,c.cname,sc.grade from S,SC,C where  sc.c#='" & cnum & "' and S.s#=SC.s# and SC.c#=C.c#"
            Else
                'sqlstring = "select s.s#,s.sname,c.c#,c.cname,sc.grade from S,SC,C where sc.s#='" & snum & "' and sc.c#='" & cnum & "' and S.s#=SC.s# and SC.c#=C.c#"
                MsgBox("至少有一项不能为空！", MsgBoxStyle.Information, "提示")
            End If
        End If
        conn.Open()
        Dim com As New SqlCommand(sqlstring, conn)
        Dim read1 As SqlDataReader = com.ExecuteReader()
        Do While read1.Read()
            Dim item As ListViewItem
            Dim subitem1, subitem2, subitem3, subitem4 As ListViewItem.ListViewSubItem
            item = New ListViewItem(read1(0).ToString)
            subitem1 = New ListViewItem.ListViewSubItem(item, read1(1))
            item.SubItems.Add(subitem1)
            subitem2 = New ListViewItem.ListViewSubItem(item, read1(2))
            item.SubItems.Add(subitem2)
            subitem3 = New ListViewItem.ListViewSubItem(item, read1(3))
            item.SubItems.Add(subitem3)
            subitem4 = New ListViewItem.ListViewSubItem(item, read1(4))
            item.SubItems.Add(subitem4)
            ListView1.Items.Add(item)
        Loop
        read1.Close()
        conn.Close()
    End Sub

    Private Sub Button2_Click(sender As System.Object, e As System.EventArgs) Handles Button2.Click
        Me.Close()
    End Sub
End Class
